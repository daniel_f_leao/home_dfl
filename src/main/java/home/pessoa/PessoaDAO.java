package home.pessoa;

import java.util.List;

public interface PessoaDAO {

	public List<Pessoa> listarTodas();
	public List<Pessoa> listarTodasResidentes();
	public Pessoa buscarPessoa(int id);
}
