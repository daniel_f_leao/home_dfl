package home.pessoa;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class PessoaHibernateDAO implements PessoaDAO, Serializable{
	
	private static final long serialVersionUID = -5677705196121870343L;
	
	private Session session;
	public void setSession(Session session) {
		this.session = session;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pessoa> listarTodas() {
		Criteria c = session.createCriteria(Pessoa.class);
		return c.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pessoa> listarTodasResidentes() {
		Criteria c = session.createCriteria(Pessoa.class);
		c.add(Restrictions.eq("residente", true));
		return c.list();
	}
	
	@Override
	public Pessoa buscarPessoa(int id) {
		return (Pessoa)this.session.get(Pessoa.class, id);
	}

}
