package home.pessoa;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity(name="pessoa")
@Table
public class Pessoa implements Serializable{

	private static final long serialVersionUID = 5377559255642320634L;
	
	@Id
	@GeneratedValue
	private int id;
	@Column(nullable=false, length=100)
	private String nome;
	@Column(nullable=true, length=100)
	private String sobrenome;
	@Column(nullable=true, length=20)
	private String telefone;
	@Column(nullable=true, length=100)
	private String email;
	@Column(nullable=false)
	private boolean residente;
	@Column(nullable=false)
	private int sexo;
	
	@Column(length=50)
	private String senha;
	@ElementCollection(targetClass = String.class) 
	@JoinTable(
			name="pessoa_permissao", 
			uniqueConstraints = {@UniqueConstraint(columnNames = {"pessoa","permissao"})}, 
			joinColumns = @JoinColumn(name = "pessoa")) 
	@Column(name = "permissao", length=50) 
	private Set<String>	permissao	= new HashSet<String>(); 
	
	public int getId() {return id;}
	public String getNome() {return nome;}
	public String getSobrenome() {return sobrenome;}
	public String getEmail() {return email;}
	public boolean isResidente() {return residente;}
	public String getTelefone() {return telefone;}
	public int getSexo(){return this.sexo;}
	
	public String getSenha() {return senha;}
	public Set<String> getPermissao() {return permissao;}
	
	public void setId(int id) {this.id = id;}
	public void setNome(String nome) {this.nome = nome;}
	public void setSobrenome(String sobrenome) {this.sobrenome = sobrenome;}
	public void setEmail(String email) {this.email = email;}
	public void setResidente(boolean residente) {this.residente = residente;}
	public void setTelefone(String telefone) {this.telefone = telefone;}
	public void setSexo(int sexo) {this.sexo = sexo;}
	
	public void setSenha(String senha) {this.senha = senha;}
	public void setPermissao(Set<String> permissao) {this.permissao = permissao;}
	public void addPermissao(String permissao){this.permissao.add(permissao);}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
