package home.beans;


import home.enums.Semana;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

@ManagedBean
@RequestScoped
public class TesterBean {

	private Date data;
	private List<Date> datas;
	private List<SelectItem> itens;
	private SelectItem item;
	private SimpleDateFormat sdf;
	private String valor;
	private List<String> valores;
	private int[] indexEscolhido;
	private Semana[] semanas = Semana.values();
	
	@PostConstruct
	public void init(){
		sdf = new SimpleDateFormat("dd/MM/yyyy");
		popularDatas();
		popularItens();
		popularValores();
	}
	
	private void popularValores(){
		valores = new ArrayList<String>();
		valores.add("valor 1");
		valores.add("valor 2");
		valores.add("valor 3");
	}
	
	private void popularItens() {
		itens = new ArrayList<SelectItem>();
		Date data = new Date();
		itens.add(new SelectItem(1, sdf.format(data)));
		
		Date data2 = new Date();
		itens.add(new SelectItem(2, sdf.format(data2)));
		
		Date data3 = new Date();
		itens.add(new SelectItem(3, sdf.format(data3)));
	}

	private void popularDatas() {
		datas = new ArrayList<Date>();
		datas.add(new Date());
		datas.add(new Date(new Date().getTime()+600000));
		datas.add(new Date(new Date().getTime()+600000));
	}
	
	public void testar(){
		System.out.println("#####chamou####");
		System.out.println(valor);
		FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	}

	public Date getData() {return data;}
	public void setData(Date data) {this.data = data;}
	public List<Date> getDatas() {return datas;}
	public void setDatas(List<Date> datas) {this.datas = datas;}
	public List<SelectItem> getItens() {return itens;}
	public void setItens(List<SelectItem> itens) {this.itens = itens;}
	public SelectItem getItem() {return item;}
	public void setItem(SelectItem item) {this.item = item;}
	public String getValor() {return valor;}
	public void setValor(String valor) {this.valor = valor;}
	public List<String> getValores() {return valores;}
	public void setValores(List<String> valores) {this.valores = valores;}
	public int[] getIndexEscolhido() {return indexEscolhido;}
	public void setIndexEscolhido(int[] indexEscolhido) {this.indexEscolhido = indexEscolhido;}
	public Semana[] getSemanas() {return semanas;}
	public void setSemanas(Semana[] semanas) {this.semanas = semanas;}
}
