package home.beans;

import home.limpador_banheiro.LimpadorBanheiro;
import home.limpador_banheiro.LimpadorBanheiroController;
import home.limpador_banheiro.LimpadorBanheiroFemController;
import home.limpador_banheiro.LimpadorBanheiroMascController;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class LimpezaBanheiroBean implements Serializable{

	private static final long serialVersionUID = -5882460665159922858L;
	private List<LimpadorBanheiro> listaMasc;
	private List<LimpadorBanheiro> listaFem;
	
	
	public List<LimpadorBanheiro> getListaMasc(){
		if(listaMasc==null){
			LimpadorBanheiroController controllerMasc = new LimpadorBanheiroMascController();
			listaMasc = controllerMasc.getTodasVezes();
		}
		return listaMasc;
	}
	
	public List<LimpadorBanheiro> getListaFem(){
		if(listaFem==null){
			LimpadorBanheiroFemController controller = new LimpadorBanheiroFemController();
			listaFem = controller.getTodasVezes();
		}
		return listaFem;
	}
}
