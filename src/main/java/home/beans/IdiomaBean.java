package home.beans;

import java.io.Serializable;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class IdiomaBean implements Serializable{

	private static final long serialVersionUID = -7569502845983216244L;
	
	private Locale local;
	
	@PostConstruct
	public void init(){
		local = FacesContext.getCurrentInstance().getViewRoot().getLocale();
	}
	
	public String setIdioma(String idioma){
		String[] separados = idioma.split("_");
		this.local = new Locale(separados[0], separados[1]);
		FacesContext.getCurrentInstance().getViewRoot().setLocale(local);
		return null;
	}
	
	public Locale getLocal() {
		return local;
	}
}
