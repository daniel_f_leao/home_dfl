package home.beans;

import home.enums.LugarLimpeza;
import home.enums.Semana;
import home.util.GerenciadorEnvioMSGBean;
import home.util.Parametros;
import home.util.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

@ManagedBean
@ApplicationScoped
public class AplicacaoBean {

	private List<Date> comboDataLimpezaNaoFeita;
	private List<SelectItem> itensComboDataLimpezaNaoFeita;
	private List<SelectItem> itensComboLugaresLimpezaNaoFeita;
	private int indexComboDataLimpezaNaoFeita;
	private String lugarLimpezaNaoFeita;
	private String lugarLimpezaFeita;
	private boolean repetirProximaSemana;
	private String idLimpezaFeita;
	private SimpleDateFormat sdf;
	//GERENCIADOR
	private GerenciadorEnvioMSGBean gerenciador;
	private String qtMaximaMensagens;
	private int[] diasDaSemanaEnviarSMS;
	private Semana[] itensDiasDaSemanaEnviarSMS;
	private Date horaEnviarMsg;
	//
	private String teste = "teste";
	
	@PostConstruct
	public void init(){
		System.out.println("*****Bean da aplicação inicializado*****");
		inicializarVariaveis();
	}
	
	private void inicializarVariaveis(){
		itensComboDataLimpezaNaoFeita = new ArrayList<SelectItem>();
		comboDataLimpezaNaoFeita = new ArrayList<Date>();
		gerenciador = new GerenciadorEnvioMSGBean();
		sdf = new SimpleDateFormat(Parametros.DATE_PATTERN);
		qtMaximaMensagens="1";
		criarListaLugaresNaoLimpados();
		if(LugarLimpeza.values().length>0)
			buscarDataLugarEscolhido(LugarLimpeza.values()[0].getCodigo());
		inicializarCheckBoxDiaSemana();
	}
	
	private void inicializarCheckBoxDiaSemana(){
		diasDaSemanaEnviarSMS = new int[7];
		itensDiasDaSemanaEnviarSMS = Semana.values();
	}

	private void criarListaLugaresNaoLimpados() {
		itensComboLugaresLimpezaNaoFeita = new ArrayList<SelectItem>();
		for(LugarLimpeza lugar :LugarLimpeza.values()){
			itensComboLugaresLimpezaNaoFeita.add(new SelectItem(lugar.getCodigo(), lugar.getLugar()));
		}
	}

	private void buscarDataLugarEscolhido(String lugarEscolhido){
		itensComboDataLimpezaNaoFeita.clear();
		comboDataLimpezaNaoFeita.clear();
		for(LugarLimpeza lugar: LugarLimpeza.values()){
			if(lugar.getCodigo().equals(lugarEscolhido)){
				comboDataLimpezaNaoFeita = lugar.getControle().cincoUltimasDatas();
				for(int i=0;i<comboDataLimpezaNaoFeita.size();i++){
					itensComboDataLimpezaNaoFeita.add(new SelectItem(i, sdf.format(comboDataLimpezaNaoFeita.get(i))));
				}
				break;
			}
		}
	}
	
	public void buscarDataLugarEscolhido(ValueChangeEvent evento){
		buscarDataLugarEscolhido(evento.getNewValue().toString());
	}
	
	public void informarNaoLimpeza(){
		for(LugarLimpeza lugar: LugarLimpeza.values()){
			if(lugar.getCodigo().equals(lugarLimpezaNaoFeita))
				lugar.getControle().informarNaoLimpeza(comboDataLimpezaNaoFeita.get(indexComboDataLimpezaNaoFeita),repetirProximaSemana);
			break;
		}
		buscarDataLugarEscolhido(lugarLimpezaNaoFeita);
	}
	
	public void informarLimpeza(){
		if(Utils.isOnlyDigits(idLimpezaFeita)){
			int id = Integer.parseInt(idLimpezaFeita);
			for(LugarLimpeza lugar: LugarLimpeza.values()){
				if(lugar.getCodigo().equals(lugarLimpezaFeita)){
					lugar.getControle().informarLimpado(id);
					limparLugarLimpezaFeita();
					break;
				}
			}
		}
	}
	
	public void ligarGerenciador(){
		Properties configurações = new Properties();
		ServletContext context = ((ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext());
		Enumeration<String> enumeration = context.getInitParameterNames();
		while(enumeration.hasMoreElements()){
			String key = enumeration.nextElement();
			configurações.put(key, context.getInitParameter(key));
		}
		if(qtMaximaMensagens!=null && !qtMaximaMensagens.isEmpty())
			configurações.put(Parametros.KEY_QT_MAX_ENVIAR_MSG, qtMaximaMensagens);
		if(horaEnviarMsg!=null)
			configurações.put(Parametros.KEY_HORA_ENVIAR_MSG, horaEnviarMsg);
		if(diasDaSemanaEnviarSMS!=null&& diasDaSemanaEnviarSMS.length>0&&diasDaSemanaEnviarSMS[0]!=0)
			configurações.put(Parametros.KEY_DIA_DA_SEMANA, diasDaSemanaEnviarSMS);
		
		gerenciador.iniciar(configurações);
	}
	
	public void desligarGerenciador(){
		gerenciador.terminar();
	}
	
	
	private void limparLugarLimpezaFeita(){
		lugarLimpezaFeita = null;
	}
	
	public String formatarData(Date date){
		return sdf.format(date);
	}
	
	public void testarAction(){
		System.out.println(lugarLimpezaNaoFeita);
	}
	
	//////////////////////////////////////////////////////////////////
	public String getTeste() {return teste;}
	public void setTeste(String teste) {this.teste = teste;}
	public GerenciadorEnvioMSGBean getGerenciador() {return gerenciador;}

	public List<Date> getComboDataLimpezaNaoFeita() {return comboDataLimpezaNaoFeita;}
	public void setComboDataLimpezaNaoFeita(List<Date> comboDataLimpezaNaoFeita) {this.comboDataLimpezaNaoFeita = comboDataLimpezaNaoFeita;}
	public List<SelectItem> getItensComboDataLimpezaNaoFeita() {return itensComboDataLimpezaNaoFeita;}
	public void setItensComboDataLimpezaNaoFeita(List<SelectItem> itensComboDataLimpezaNaoFeita) {this.itensComboDataLimpezaNaoFeita = itensComboDataLimpezaNaoFeita;}
	public int getIndexComboDataLimpezaNaoFeita() {return indexComboDataLimpezaNaoFeita;}
	public void setIndexComboDataLimpezaNaoFeita(int indexComboDataLimpezaNaoFeita) {this.indexComboDataLimpezaNaoFeita = indexComboDataLimpezaNaoFeita;}
	public List<SelectItem> getItensComboLugaresLimpezaNaoFeita() {return itensComboLugaresLimpezaNaoFeita;}
	public void setItensComboLugaresLimpezaNaoFeita(List<SelectItem> itensComboLugaresLimpezaNaoFeita) {this.itensComboLugaresLimpezaNaoFeita = itensComboLugaresLimpezaNaoFeita;}
	public boolean isRepetirProximaSemana() {return repetirProximaSemana;}
	public void setRepetirProximaSemana(boolean repetirProximaSemana) {this.repetirProximaSemana = repetirProximaSemana;}
	public String getIdLimpezaFeita() {return idLimpezaFeita;}
	public void setIdLimpezaFeita(String idLimpezaFeita) {this.idLimpezaFeita = idLimpezaFeita;}
	public String getLugarLimpezaNaoFeita() {return lugarLimpezaNaoFeita;}
	public void setLugarLimpezaNaoFeita(String lugarLimpezaNaoFeita) {this.lugarLimpezaNaoFeita = lugarLimpezaNaoFeita;}
	public String getLugarLimpezaFeita() {return lugarLimpezaFeita;}
	public void setLugarLimpezaFeita(String lugarLimpezaFeita) {this.lugarLimpezaFeita = lugarLimpezaFeita;}
	public String getQtMaximaMensagens() {return qtMaximaMensagens;}
	public void setQtMaximaMensagens(String qtMaximaMensagens) {this.qtMaximaMensagens = qtMaximaMensagens;}
	public int[] getDiasDaSemanaEnviarSMS() {return diasDaSemanaEnviarSMS;}
	public void setDiasDaSemanaEnviarSMS(int[] diasDaSemanaEnviarSMS) {this.diasDaSemanaEnviarSMS = diasDaSemanaEnviarSMS;}
	public Date getHoraEnviarMsg() {return horaEnviarMsg;}
	public void setHoraEnviarMsg(Date horaEnviarMsg) {this.horaEnviarMsg = horaEnviarMsg;}
	public Semana[] getItensDiasDaSemanaEnviarSMS() {return itensDiasDaSemanaEnviarSMS;}
	public void setItensDiasDaSemanaEnviarSMS(Semana[] itensDiasDaSemanaEnviarSMS) {this.itensDiasDaSemanaEnviarSMS = itensDiasDaSemanaEnviarSMS;}
}
