package home.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import home.limpadores_casa.LimpadoresCasa;
import home.limpadores_casa.LimpadoresCasaController;
import home.util.FormatadorNome;

@ManagedBean
@RequestScoped
public class LimpezaCasaBean implements Serializable{

	private static final long serialVersionUID = -5882460665159922858L;
	private List<LimpadoresCasa> lista;
	
	
	public List<LimpadoresCasa> getLista(){
		if(lista==null){
			LimpadoresCasaController controller = new LimpadoresCasaController();
			lista = controller.getTodasLimpadas();
		}
		return lista;
	}
	
	public String formatadorNomes(LimpadoresCasa limpCasa){
		return FormatadorNome.formatarNomesDupla(limpCasa);
	}
}
