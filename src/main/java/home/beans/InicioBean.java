package home.beans;

import home.limpador_banheiro.LimpadorBanheiro;
import home.limpador_banheiro.LimpadorBanheiroController;
import home.limpador_banheiro.LimpadorBanheiroFemController;
import home.limpador_banheiro.LimpadorBanheiroMascController;
import home.limpadores_casa.LimpadoresCasa;
import home.limpadores_casa.LimpadoresCasaController;
import home.util.FormatadorNome;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class InicioBean implements Serializable{

	private static final long serialVersionUID = 3459842494499247884L;
	
	private LimpadoresCasa limpadoresCasa;
	private LimpadorBanheiro masc;
	private LimpadorBanheiro fem;
	@ManagedProperty(value="#{idiomaBean}")
	private IdiomaBean idiomaBean;
	
	@PostConstruct
	public void init(){
		LimpadoresCasaController limpCasaController = new LimpadoresCasaController();
		LimpadorBanheiroController limpBanheiroMascController = new LimpadorBanheiroMascController();
		LimpadorBanheiroController limpBanheiroFemController = new LimpadorBanheiroFemController();
		limpadoresCasa = limpCasaController.proximoLimparCasa();
		masc = limpBanheiroMascController.proximoLimpar();
		fem = limpBanheiroFemController.proximoLimpar();
//		if(idiomaBean!=null && idiomaBean.getLocal()!=null)
//			FacesContext.getCurrentInstance().getViewRoot().setLocale(idiomaBean.getLocal());
	}
	
	public String formatadorNomes(LimpadoresCasa limpadores){
		return FormatadorNome.formatarNomesDupla(limpadores);
	}
	
	public String formatadorNomes(){
		return FormatadorNome.formatarNomesDupla(limpadoresCasa);
	}
	
	
	
	public LimpadoresCasa getLimpadoresCasa() {return limpadoresCasa;}
	public LimpadorBanheiro getMasc() {return masc;}
	public LimpadorBanheiro getFem() {return fem;}
	public IdiomaBean getIdiomaBean() {return idiomaBean;}
	public void setLimpadoresCasa(LimpadoresCasa limpadoresCasa) {this.limpadoresCasa = limpadoresCasa;}
	public void setMasc(LimpadorBanheiro masc) {this.masc = masc;}
	public void setFem(LimpadorBanheiro fem) {this.fem = fem;}
	public void setIdiomaBean(IdiomaBean idiomaBean) {this.idiomaBean = idiomaBean;}
}
