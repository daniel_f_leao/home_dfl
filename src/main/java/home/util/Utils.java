package home.util;

public class Utils {

	public static boolean isOnlyDigits(String conjuntoCaracteres){
		if(conjuntoCaracteres != null && !conjuntoCaracteres.isEmpty()){
			conjuntoCaracteres = conjuntoCaracteres.trim();
			for(char c : conjuntoCaracteres.toCharArray()){
				if(!Character.isDigit(c)){
					return false;
				}
			}
			return true;
		}
		return false;
	}
}
