package home.util;

import java.io.Serializable;

import home.limpadores_casa.LimpadoresCasa;
import home.pessoa.Pessoa;

public class FormatadorNome implements Serializable{

	private static final long serialVersionUID = -7851939653117967964L;

	public static String formatarNomesDupla(LimpadoresCasa limpCasa){
		String dupla = "";
		String operadorE = MsgUtils.getMensagem("operador_e");
		if(limpCasa != null){
			Pessoa p1 = limpCasa.getP1();
			Pessoa p2 = limpCasa.getP2();
			if(p1 != null){
				dupla+= p1.getNome();
			}
			if(p2!=null){
				if(!dupla.isEmpty())
					dupla+= " " + operadorE + " ";
				dupla+=p2.getNome();
			}
		}
		return dupla;
	}
}
