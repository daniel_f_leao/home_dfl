package home.util;

import home.hibernate.HibernateFactory;
import home.pessoa.PessoaHibernateDAO;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.joda.time.DateTime;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.Message.Status;
import com.twilio.type.PhoneNumber;

public class GerenciadorEnvioMSGBean implements Runnable {

	private static final int DIA_DA_SEMANA = Calendar.DAY_OF_WEEK;
	private static final int COOLDOWN = 30000;
	private static final int TEMPO_ESPERA_VERIFICAR_MSG_FOI_ENVIADA = 30000;	
	private static final String NOME_THREAD = "Thread Enviadora de Msg";
	private static final int QT_MAX_THREADS_ENVIADORAS_MSG = 5;

	private volatile int qtMaxPermitidaMsgEnviada = 2;
	private Date horaLiberada;
	private int[] diaDeEnviarSms;
	
	private List<PessoaWrapper> compromissadosDesavisados;
	private volatile boolean continuar;
	private boolean ligado;
	private String accountSid;
	private String authToken;
	private String myNumber;
	private Thread threadGerenciador;
	private ExecutorService poolThreads;
	private int numUltimoDiaMsgEnviada;
	private List<Message> mensagensEnviadas;

	public boolean isLigado() {return ligado;}
	public List<Message> getMensagensEnviadas() {return mensagensEnviadas;}
	
	private boolean horarioPermitido(){
		Calendar c = Calendar.getInstance();
		for(int dia : diaDeEnviarSms){
			if(c.get(DIA_DA_SEMANA) == dia){
				Calendar horaLiberadaDoDia = Calendar.getInstance();
				horaLiberadaDoDia.setTime(horaLiberada);
				if(c.get(Calendar.HOUR_OF_DAY) >= horaLiberadaDoDia.get(Calendar.HOUR_OF_DAY)){
					return true;
				}
			}
		}
		return false;	
	}
	
	private boolean ehDiaDiferente(int diaAtual){
		if(diaAtual != numUltimoDiaMsgEnviada ){
			return true;
		}
		return false;
	}
	
	private ExecutorService createNewFixedExecutors(){
		ThreadFactoryBuilder builder = new ThreadFactoryBuilder();
		builder.setNameFormat(NOME_THREAD);
		return Executors.newFixedThreadPool(QT_MAX_THREADS_ENVIADORAS_MSG, builder.build());
	}

	@Override
	public void run() {
		while (continuar) {
			try {
				int numDiaAtual = Calendar.getInstance().get(
						Calendar.DAY_OF_YEAR);
				if (ehDiaDiferente(numDiaAtual) && horarioPermitido()) {
					numUltimoDiaMsgEnviada = numDiaAtual;
					atualizarCompromissados();
					Iterator<PessoaWrapper> iPessoa = compromissadosDesavisados
							.iterator();
					while (iPessoa.hasNext()) {
						PessoaWrapper p = iPessoa.next();
						if(p!= null && p.getP1().getTelefone() != null && !p.getP1().getTelefone().isEmpty()){
							poolThreads.execute(new TarefaEnviarSMS(p));
						}
					}
				}
				Thread.sleep(COOLDOWN);
			} 
			catch(InterruptedException|SecurityException IEx){
				if(continuar){
					IEx.printStackTrace();
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public void iniciar(Properties configuracoes) {
		buscarParametros(configuracoes);
		inicializarVariaveis();
		Twilio.init(accountSid, authToken);
		threadGerenciador.start();
		System.out.println("*********Gerenciador de Envio de SMS Iniciado*********");
	}
	
	private void inicializarVariaveis(){
		consertarParaValoresPadroes();
		compromissadosDesavisados = new ArrayList<PessoaWrapper>();
		mensagensEnviadas = Collections.checkedList(new ArrayList<Message>(), Message.class);
		threadGerenciador = new Thread(this);
		threadGerenciador.setName("Gerenciador de Envio de MSG");
		poolThreads = createNewFixedExecutors();
		continuar = true;
		ligado = true;
	}
	
	private void consertarParaValoresPadroes(){
		if(diaDeEnviarSms==null){
			diaDeEnviarSms= new int[1];
			diaDeEnviarSms[0]=6;
		}
		if(qtMaxPermitidaMsgEnviada==0){
			qtMaxPermitidaMsgEnviada=1;
		}
		if(horaLiberada==null){
			Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 18);
			c.set(Calendar.MINUTE, 0);
			horaLiberada = c.getTime();
		}
		
	}
	
	private void buscarParametros(Properties configuracoes){
		accountSid = configuracoes.getProperty(Parametros.ACCOUNT_SID);
		authToken = configuracoes.getProperty(Parametros.AUTH_TOKEN);
		myNumber = configuracoes.getProperty(Parametros.SMS_MY_NUMBER);
		qtMaxPermitidaMsgEnviada =Integer.parseInt(configuracoes.getProperty(Parametros.KEY_QT_MAX_ENVIAR_MSG));
		diaDeEnviarSms = (int[]) configuracoes.get(Parametros.KEY_DIA_DA_SEMANA);
		horaLiberada = (Date) configuracoes.get(Parametros.KEY_HORA_ENVIAR_MSG);
	}

	public void terminar() {
		continuar=false;
		threadGerenciador.interrupt();
		Enumeration<Driver> drivers = DriverManager.getDrivers();
        Driver d = null;
        while(drivers.hasMoreElements()) {
            try {
                d = drivers.nextElement();
                DriverManager.deregisterDriver(d);
                System.out.println("Driver %s deregistered " + d);
            } catch (SQLException ex) {
                System.out.println("Error deregistering driver " + d);
            }
        }
        if(!poolThreads.isTerminated()||!poolThreads.isShutdown()){
        	poolThreads.shutdownNow();
        }
		ligado=false;
	}

	private String criarMensagemAvisoLimpeza(String nomePessoa, String funcao){
		String mensagem = "Olá " + nomePessoa + ". Não se esqueça que esse final de semana você é responsável por limpar" + funcao;
		return mensagem;
	}

	private void atualizarCompromissados() {
		
		try{
			Session session = HibernateFactory.getSession().getCurrentSession();
			Transaction transaction = session.beginTransaction();
			compromissadosDesavisados.clear();
			PessoaHibernateDAO dao = new PessoaHibernateDAO();
			dao.setSession(session);
			compromissadosDesavisados.add(new PessoaWrapper(dao.buscarPessoa(1)," a Casa"));
			transaction.commit();
			if(session.isOpen())
				session.close();
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
//		PessoaDAO dao = DAOFactory.getPessoaDAO();
//		compromissadosDesavisados.add(new PessoaWrapper(dao.buscarPessoa(1)," a Casa"));
		/*
		LimpadorBanheiroController banController = new LimpadorBanheiroController();
		LimpadoresCasaController casaController = new LimpadoresCasaController();
		
		compromissadosDesavisados.add(banController.proximaMulherLimpar()
				.getP1());
		compromissadosDesavisados.add(banController.proximoHomemLimpar()
				.getP1());
		compromissadosDesavisados.add(casaController.proximoLimparCasa()
				.getP1());
		compromissadosDesavisados.add(casaController.proximoLimparCasa()
				.getP2());*/
	}

class TarefaEnviarSMS implements Runnable{
		
		private PessoaWrapper p;
		private volatile boolean recebido;
		private List<Message> msgEnviadas;
		
		public TarefaEnviarSMS(PessoaWrapper p) {
			this.p = p;
			msgEnviadas = Collections.checkedList(new ArrayList<Message>(), Message.class);
		}
		
		private Message enviarSMS(String to, String msg) {
			Message msgCriada = Message.creator(new PhoneNumber(to), new PhoneNumber(myNumber), msg).create();
			System.out.println("Mensagem enviada: " + msgCriada.getSid());
			mensagensEnviadas.add(msgCriada);
			return msgCriada;
		}
		
		@Override
		public void run() {
			String nomeThread =null;
			try{
				nomeThread = Thread.currentThread().getName();
				while(!recebido && msgEnviadas.size() <= qtMaxPermitidaMsgEnviada){
					Message ultimaMsgEnviada = enviarSMS(p.getP1().getTelefone(), criarMensagemAvisoLimpeza(p.getP1().getNome(),p.getFuncao()));
					msgEnviadas.add(ultimaMsgEnviada);
					Thread.sleep(TEMPO_ESPERA_VERIFICAR_MSG_FOI_ENVIADA);
					for(Message mensagem: msgEnviadas){
						Message mensagemAtualizada = Message.fetcher(mensagem.getSid()).fetch();
						if(mensagemAtualizada.getStatus().equals(Status.DELIVERED)){
							recebido = true;
							break;
						}
					}
				}
			}
			catch(Exception ex){
				System.out.println("ERRO NA THREAD: " + nomeThread + ". " + ex.getMessage() );
				ex.printStackTrace();
			}
		}
		
	}

	public List<Message> getListaAtualizadaDeMsgEnviadas(){
		if(mensagensEnviadas!=null){
			List<Message> mensagens = new ArrayList<Message>();
			for(Message msg : mensagensEnviadas){
				mensagens.add(Message.fetcher(msg.getSid()).fetch());
			}
			return mensagens;
		}
		return null;
	}
	
	public String dateTimeToString(DateTime datetime){
		if(datetime!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			sdf.setTimeZone(TimeZone.getTimeZone("Australia/Sydney"));
			return sdf.format(datetime.toDate());
		}
		return "";
	}

}