package home.util;

import home.beans.AplicacaoBean;

public class Parametros {

	public static final String ACCOUNT_SID= "twili_account_sid";
	public static final String AUTH_TOKEN= "twili_auth_token";
	public static final String DATE_PATTERN = "dd/MM/yyyy";
	public static final String GERENCIADOR_ENVIO_SMS_OBJ = "gerenciador_envio_sms_obj";
	public static final String KEY_DIA_DA_SEMANA = "diaDaSemana";
	public static final String KEY_HORA_ENVIAR_MSG = "horaEnviarMsg";
	public static final String KEY_QT_MAX_ENVIAR_MSG = "keyQtMaxEnviarMsg";
	public static final String NOME_ADMINISTRADOR_APLICACAO_BEAN = "aplicacaoBean";
	public static final int PRIMEIRA_POSICAO_VETOR = 0;
	public static final String SMS_MY_NUMBER= "twili_my_number";
	public static final Class<?> TIPO_ADMINISTRADOR_APLICACAO_BEAN = AplicacaoBean.class;
}
