package home.util;

import home.hibernate.HibernateFactory;
import home.pessoa.PessoaHibernateDAO;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.Message.Status;
import com.twilio.type.PhoneNumber;

public class GerenciadorEnvioMSG implements ServletContextListener, Runnable {

	private static final int DIA_DE_ENVIAR_SMS = Calendar.SATURDAY;
	private static final int HORA_LIBERADA = 17;
	private static final int DIA_DA_SEMANA = Calendar.DAY_OF_WEEK;
	private static final int COOLDOWN = 600000;
	private static final int TEMPO_ESPERA_VERIFICAR_MSG_FOI_ENVIADA = 30000;
	private static final int QT_MAX_PERMITIDA_MSG_ENVIADA = 2;
	private static final String NOME_THREAD = "Thread Enviadora de Msg";
	private static final int QT_MAX_THREADS_ENVIADORAS_MSG = 5;

	private List<PessoaWrapper> compromissadosDesavisados; 
	private ServletContextEvent evento;
	private boolean continuar;
	private String accountSid;
	private String authToken;
	private String myNumber;
	private Thread threadGerenciador;
	private ExecutorService poolThreads;
	private int numUltimaSemanaMsgEnviada;

	public ServletContextEvent getEvento() {return evento;}
	public void setEvento(ServletContextEvent evento) {this.evento = evento;}
	
	private boolean horarioPermitido(){
		Calendar c = Calendar.getInstance();
		if(c.get(DIA_DA_SEMANA) == DIA_DE_ENVIAR_SMS && c.get(Calendar.HOUR_OF_DAY) >= HORA_LIBERADA){
			return true;
		}
		return false;	
	}
	
	private boolean ehSemanaDiferente(int semanaAtual){
		if(semanaAtual != numUltimaSemanaMsgEnviada ){
			return true;
		}
		return false;
	}
	
	private ExecutorService createNewFixedExecutors(){
		ThreadFactoryBuilder builder = new ThreadFactoryBuilder();
		builder.setNameFormat(NOME_THREAD);
		return Executors.newFixedThreadPool(QT_MAX_THREADS_ENVIADORAS_MSG, builder.build());
	}

	@Override
	public void run() {
		// PRE LÁÇO
		{
			System.out
					.println("*********Gerenciador de Envio de SMS Iniciado*********");
			Twilio.init(accountSid, authToken);
			compromissadosDesavisados = new ArrayList<PessoaWrapper>();
			poolThreads = createNewFixedExecutors();
			continuar = true;
		}

		while (continuar) {
			try {
				int numSemanaAtual = Calendar.getInstance().get(
						Calendar.WEEK_OF_YEAR);
				if (ehSemanaDiferente(numSemanaAtual) && horarioPermitido()) {
					numUltimaSemanaMsgEnviada = numSemanaAtual;
					atualizarCompromissados();
					Iterator<PessoaWrapper> iPessoa = compromissadosDesavisados
							.iterator();
					while (iPessoa.hasNext()) {
						PessoaWrapper p = iPessoa.next();
						if(p!= null && p.getP1().getTelefone() != null && !p.getP1().getTelefone().isEmpty()){
							poolThreads.execute(new TarefaEnviarSMS(p));
						}
					}
				}
				Thread.sleep(COOLDOWN);
			} 
			catch(InterruptedException|SecurityException IEx){
				if(continuar){
					IEx.printStackTrace();
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public void contextInitialized(ServletContextEvent evento) {
		setEvento(evento);
		accountSid = evento.getServletContext().getInitParameter(Parametros.ACCOUNT_SID);
		authToken = evento.getServletContext().getInitParameter(Parametros.AUTH_TOKEN);
		myNumber = evento.getServletContext().getInitParameter(Parametros.SMS_MY_NUMBER);
		evento.getServletContext().setAttribute(Parametros.GERENCIADOR_ENVIO_SMS_OBJ, this);
		threadGerenciador = new Thread(this);
		threadGerenciador.setName("Gerenciador de Envio de MSG");
		threadGerenciador.start();
	}

	public void contextDestroyed(ServletContextEvent arg0) {
		continuar=false;
		threadGerenciador.interrupt();
		Enumeration<Driver> drivers = DriverManager.getDrivers();
        Driver d = null;
        while(drivers.hasMoreElements()) {
            try {
                d = drivers.nextElement();
                DriverManager.deregisterDriver(d);
                System.out.println("Driver %s deregistered " + d);
            } catch (SQLException ex) {
                System.out.println("Error deregistering driver " + d);
            }
        }
        if(!poolThreads.isTerminated()||!poolThreads.isShutdown()){
        	poolThreads.shutdownNow();
        }
		System.out.println("ServletContextListener destroyed");
	}

	private Message enviarSMS(String to, String msg) {
		Message msgCriada = Message.creator(new PhoneNumber(to), new PhoneNumber(myNumber), msg).create();
		System.out.println("Mensagem enviada: " + msgCriada.getSid());
		return msgCriada;
	}

	private String criarMensagemAvisoLimpeza(String nomePessoa, String funcao){
		String mensagem = "Olá " + nomePessoa + ". Não se esqueça que esse final de semana você é responsável por limpar" + funcao;
		return mensagem;
	}

	private void atualizarCompromissados() {
		
		try{
			Session session = HibernateFactory.getSession().getCurrentSession();
			Transaction transaction = session.beginTransaction();
			compromissadosDesavisados.clear();
			PessoaHibernateDAO dao = new PessoaHibernateDAO();
			dao.setSession(session);
			compromissadosDesavisados.add(new PessoaWrapper(dao.buscarPessoa(1)," a Casa"));
			transaction.commit();
			if(session.isOpen())
				session.close();
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		
		/*
		LimpadorBanheiroController banController = new LimpadorBanheiroController();
		LimpadoresCasaController casaController = new LimpadoresCasaController();
		
		compromissadosDesavisados.add(banController.proximaMulherLimpar()
				.getP1());
		compromissadosDesavisados.add(banController.proximoHomemLimpar()
				.getP1());
		compromissadosDesavisados.add(casaController.proximoLimparCasa()
				.getP1());
		compromissadosDesavisados.add(casaController.proximoLimparCasa()
				.getP2());*/
	}

class TarefaEnviarSMS implements Runnable{
		
		private PessoaWrapper p;
		private boolean recebido;
		private List<Message> msgEnviadas;
		
		public TarefaEnviarSMS(PessoaWrapper p) {
			this.p = p;
			msgEnviadas = new ArrayList<Message>();
		}
		
		@Override
		public void run() {
			String nomeThread =null;
			try{
				nomeThread = Thread.currentThread().getName();
				while(!recebido && msgEnviadas.size() <= QT_MAX_PERMITIDA_MSG_ENVIADA){
					Message ultimaMsgEnviada = enviarSMS(p.getP1().getTelefone(), criarMensagemAvisoLimpeza(p.getP1().getNome(),p.getFuncao()));
					msgEnviadas.add(ultimaMsgEnviada);
					Thread.sleep(TEMPO_ESPERA_VERIFICAR_MSG_FOI_ENVIADA);
					for(Message mensagem: msgEnviadas){
						Message mensagemAtualizada = Message.fetcher(mensagem.getSid()).fetch();
						if(mensagemAtualizada.getStatus().equals(Status.DELIVERED)){
							recebido = true;
							break;
						}
					}
				}
			}
			catch(Exception ex){
				System.out.println("ERRO NA THREAD: " + nomeThread + ". " + ex.getMessage() );
				ex.printStackTrace();
			}
		}
		
	}

}