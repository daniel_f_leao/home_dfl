package home.util;

import home.pessoa.Pessoa;

public class PessoaWrapper {

	public PessoaWrapper() {}
	public PessoaWrapper(Pessoa p){this.p1 = p;}
	public PessoaWrapper(Pessoa p, String funcao){
		this.funcao = funcao;
		this.p1 = p;
	}
	
	private Pessoa p1;
	private String funcao;
	public Pessoa getP1() {return p1;}
	public void setP1(Pessoa p1) {this.p1 = p1;}
	public String getFuncao() {return funcao;}
	public void setFuncao(String funcao) {this.funcao = funcao;}
	
	
}
