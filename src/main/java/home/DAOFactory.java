package home;

import java.io.Serializable;

import home.hibernate.HibernateFactory;
import home.limpador_banheiro.LimpadorBanheiroDAO;
import home.limpador_banheiro.LimpadorBanheiroHibernateDAO;
import home.limpadores_casa.LimpadoresCasaDAO;
import home.limpadores_casa.LimpadoresCasaHibernateDAO;
import home.pessoa.PessoaDAO;
import home.pessoa.PessoaHibernateDAO;

public class DAOFactory implements Serializable{

	private static final long serialVersionUID = 2199050878224508091L;

	public static PessoaDAO getPessoaDAO(){
		PessoaHibernateDAO pd = new PessoaHibernateDAO();
		pd.setSession(HibernateFactory.getSession().getCurrentSession());
		return pd;
	}
	
	public static LimpadoresCasaDAO getLimpadoresCasaDAO(){
		LimpadoresCasaHibernateDAO dao = new LimpadoresCasaHibernateDAO();
		dao.setSession(HibernateFactory.getSession().getCurrentSession());
		return dao;
	}
	
	public static LimpadorBanheiroDAO getLimpadorBanheiro(){
		LimpadorBanheiroHibernateDAO dao = new LimpadorBanheiroHibernateDAO();
		dao.setSession(HibernateFactory.getSession().getCurrentSession());
		return dao;
	}
	
}
