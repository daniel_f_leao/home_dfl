package home.limpador_banheiro;

import home.pessoa.Pessoa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class LimpadorBanheiroHibernateDAO implements LimpadorBanheiroDAO, Serializable{
	
	private static final long serialVersionUID = -443999879896698199L;
	private Session session;
	
	public void setSession(Session session) {
		this.session = session;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LimpadorBanheiro> cincoUltimasVezes(int sexo) {
		Criteria c = session.createCriteria(LimpadorBanheiro.class);
		c.add(Restrictions.le("data", new Date()));
		return c.addOrder(Order.desc("data")).add(Restrictions.eq("sexo", sexo)).setMaxResults(5).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LimpadorBanheiro> todasVezes(int sexo) {
		return session.createCriteria(LimpadorBanheiro.class).addOrder(Order.desc("data")).add(Restrictions.eq("sexo", sexo)).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LimpadorBanheiro> cincoUltimasVezesDaPessoa(Pessoa p1) {
		Criteria c = session.createCriteria(LimpadorBanheiro.class);
		c.add(Restrictions.le("data", new Date()));
		return c.add(Restrictions.eq("p1", p1.getId())).addOrder(Order.desc("data")).setMaxResults(5).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LimpadorBanheiro> todasVezesDaPessoa(Pessoa p1) {
		Criteria c = session.createCriteria(LimpadorBanheiro.class);
		return c.add(Restrictions.eq("p1", p1.getId())).addOrder(Order.desc("data")).list();
	}
	
	@Override
	public LimpadorBanheiro buscarPorData(Date data, int sexo) {
		Criteria c = session.createCriteria(LimpadorBanheiro.class);
		return (LimpadorBanheiro) c.add(Restrictions.eq("data", data)).add(Restrictions.eq("sexo", sexo)).uniqueResult();
	}
	
	@Override
	public void atualizarLimpadoresCasa(LimpadorBanheiro limpador) {
		session.saveOrUpdate(limpador);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<LimpadorBanheiro> proximasVezes(Date date, int sexo) {
		Criteria c = session.createCriteria(LimpadorBanheiro.class);
		c.add(Restrictions.gt("data", date));
		c.add(Restrictions.eq("sexo", sexo));
		c.addOrder(Order.asc("data"));
		return c.list();
	}
	
	@Override
	public LimpadorBanheiro buscarPorId(int id) {
		return (LimpadorBanheiro) session.get(LimpadorBanheiro.class, id);
	}
}
