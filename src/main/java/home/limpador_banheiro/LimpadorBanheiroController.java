package home.limpador_banheiro;

import home.DAOFactory;
import home.interfaces.DefinidoresLimpezaController;
import home.pessoa.Pessoa;
import home.util.Parametros;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public abstract class LimpadorBanheiroController implements DefinidoresLimpezaController{

	protected LimpadorBanheiroDAO dao;
	
	protected LimpadorBanheiroController() {
		dao = DAOFactory.getLimpadorBanheiro();
	}
	
	public abstract LimpadorBanheiro proximoLimpar();
	public abstract List<LimpadorBanheiro> getCincoUltimasVezes();
	public abstract List<LimpadorBanheiro> getTodasVezes();
	
	protected void informarNaoLimpeza(Date data, int sexo, boolean repetir){
		LimpadorBanheiro limpador=dao.buscarPorData(data, sexo);
		limpador.setLimpado(false);
		dao.atualizarLimpadoresCasa(limpador);
		if(repetir){
			List<LimpadorBanheiro> lista = dao.proximasVezes(data, sexo);
			for(LimpadorBanheiro limpadores: lista){
				Calendar c = Calendar.getInstance();
				c.setTime(limpadores.getData());
				c.add(Calendar.DAY_OF_YEAR, 7);
				limpadores.setData(c.getTime());
				dao.atualizarLimpadoresCasa(limpadores);
			}
		}
	}
	
	protected List<LimpadorBanheiro> getTodasVezes(int sexo){
		return dao.todasVezes(sexo);
	}
	
	protected LimpadorBanheiro proximoLimpar(int sexo){
//		Calendar c = Calendar.getInstance();
//		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);//ir para o inicio da semana(Domingo)
//		c.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR)+6);//soma o dia do ano + 6(próximo sabado)
//		LimpadorBanheiro retorno = dao.buscarPorData(c.getTime(), sexo);
//		if(retorno == null){
//			c.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR)+1);//soma o dia do ano + 1(domingo)
//			dao.buscarPorData(c.getTime(), sexo);
//		}
		List<LimpadorBanheiro> lista=dao.proximasVezes(Calendar.getInstance().getTime(), sexo);
		LimpadorBanheiro retorno = null; 
		if(lista!=null&&!lista.isEmpty())
			retorno = lista.get(Parametros.PRIMEIRA_POSICAO_VETOR);
		return retorno;
	}
	
	protected List<Date> cincoUltimasDatas(int sexo){
		List<Date> datas = new ArrayList<Date>();
		List<LimpadorBanheiro> limpadores = getCincoUltimasVezes(sexo);
		for(LimpadorBanheiro limpador: limpadores){
			datas.add(limpador.getData());
		}
		return datas;
	} 
	
	protected List<LimpadorBanheiro> getCincoUltimasVezes(int sexo){
		return dao.cincoUltimasVezes(sexo);
	}
	
	@Override
	public void informarLimpado(int id) {
		LimpadorBanheiro limpador = dao.buscarPorId(id);
		if(limpador != null){
			limpador.setLimpado(true);
			dao.atualizarLimpadoresCasa(limpador);
		}
	}
	
	public List<LimpadorBanheiro> getCincoUltimasVezesDaPessoa(Pessoa p1) {
		return dao.cincoUltimasVezesDaPessoa(p1);
	}
	
	public List<LimpadorBanheiro> todasVezesDaPessoa(Pessoa p1) {
		return dao.todasVezesDaPessoa(p1);
	}
}
