package home.limpador_banheiro;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class LimpadorBanheiroMascController extends LimpadorBanheiroController implements Serializable{
	
	private static final long serialVersionUID = -7998610036790477078L;
	private static final int MASC = 1;
	
	public LimpadorBanheiroMascController() {
		super();
	}
	
	@Override
	public void informarNaoLimpeza(Date date, boolean repetir) {
		informarNaoLimpeza(date, MASC, repetir);
	}
	
	@Override
	public List<Date> cincoUltimasDatas() {
		return cincoUltimasDatas(MASC);
	}
	@Override
	public List<LimpadorBanheiro> getCincoUltimasVezes() {
		return getCincoUltimasVezes(MASC);
	}
	@Override
	public List<LimpadorBanheiro> getTodasVezes() {
		return getTodasVezes(MASC);
	}
	
	@Override
	public LimpadorBanheiro proximoLimpar() {
		return proximoLimpar(MASC);
	}
}
