package home.limpador_banheiro;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import home.pessoa.Pessoa;

@Entity(name="limpador_banheiro")
@Table
public class LimpadorBanheiro implements Serializable{

	private static final long serialVersionUID = 4961541595618634863L;
	
	@Id
	@GeneratedValue
	private int id;
	@OneToOne
	private Pessoa p1;
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date data;
	@Column(nullable=false)
	private boolean limpado;
	@Column(nullable=false)
	private int sexo;
	
	
	public int getId() {return id;}
	public Pessoa getP1() {return p1;}
	public Date getData() {return data;}
	public boolean isLimpado(){return limpado;}
	public int getSexo() {return sexo;}
	
	public void setId(int id) {this.id = id;}
	public void setP1(Pessoa p1) {this.p1 = p1;}
	public void setData(Date data) {this.data = data;}
	public void setLimpado(boolean limpado) {this.limpado = limpado;}
	public void setSexo(int sexo) {this.sexo = sexo;}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LimpadorBanheiro other = (LimpadorBanheiro) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
