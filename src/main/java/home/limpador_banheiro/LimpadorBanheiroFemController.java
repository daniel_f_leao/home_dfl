package home.limpador_banheiro;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class LimpadorBanheiroFemController extends LimpadorBanheiroController implements Serializable{
	
	private static final long serialVersionUID = 5147100097386298430L;
	private static final int FEM = 0;
	
	public LimpadorBanheiroFemController() {
		super();
	}
	
	@Override
	public void informarNaoLimpeza(Date date, boolean repetir) {
		informarNaoLimpeza(date, FEM, repetir);
	}
	
	@Override
	public List<Date> cincoUltimasDatas() {
		return cincoUltimasDatas(FEM);
	}
	@Override
	public List<LimpadorBanheiro> getCincoUltimasVezes() {
		return getCincoUltimasVezes(FEM);
	}
	@Override
	public List<LimpadorBanheiro> getTodasVezes() {
		return dao.todasVezes(FEM);
	}
	
	@Override
	public LimpadorBanheiro proximoLimpar() {
		return proximoLimpar(FEM);
	}
}
