package home.limpador_banheiro;

import home.pessoa.Pessoa;

import java.util.Date;
import java.util.List;

public interface LimpadorBanheiroDAO {
	public List<LimpadorBanheiro> cincoUltimasVezes(int sexo);
	public List<LimpadorBanheiro> todasVezes(int sexo);
	public List<LimpadorBanheiro> cincoUltimasVezesDaPessoa(Pessoa p1);
	public List<LimpadorBanheiro> todasVezesDaPessoa(Pessoa p1);
	public LimpadorBanheiro buscarPorData(Date data, int sexo);
	public List<LimpadorBanheiro> proximasVezes(Date date, int sexo);
	public void atualizarLimpadoresCasa(LimpadorBanheiro limpador);
	public LimpadorBanheiro buscarPorId(int id);
}
