package home.enums;

import home.interfaces.DefinidoresLimpezaController;
import home.limpador_banheiro.LimpadorBanheiroFemController;
import home.limpador_banheiro.LimpadorBanheiroMascController;
import home.limpadores_casa.LimpadoresCasaController;

public enum LugarLimpeza {
	
	CASA("Casa", "casa", LimpadoresCasaController.class),
	BANHEIRO_MASC("Banheiro Masc.", "banmasc", LimpadorBanheiroMascController.class),
	BANHEIRO_FEM("Banheiro Fem.", "banfem", LimpadorBanheiroFemController.class);

	private String lugar;
	private String codigo;
	private Class<? extends DefinidoresLimpezaController> controleClass;
	private LugarLimpeza(String lugar, String codigo, Class<? extends DefinidoresLimpezaController> naoLimpado) {
		this.lugar=lugar;
		this.codigo=codigo;
		this.controleClass=naoLimpado;
	}
	public DefinidoresLimpezaController getControle() {
		try{
			return controleClass.newInstance();
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println("ERRO No método getControle do ENUM");
			return null;
		}
	}
	public String getLugar() {return lugar;}
	public String getCodigo(){return codigo;}

}
