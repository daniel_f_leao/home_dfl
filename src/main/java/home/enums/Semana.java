package home.enums;

public enum Semana {

	DOMINGO(1, "Domingo", "Domingo", "domingo"),
	SEGUNDA(2, "Segunda-Feira", "Segunda", "segunda"),
	TERCA(3,"Terça-Feira", "Terça", "terca"),
	QUARTA(4,"Quarta-Feira","Quarta", "quarta"),
	QUINTA(5,"Quinta-Feira","Quinta", "quinta"),
	SEXTA(6,"Sexta-Feira","Sexta", "sexta"),
	SABADO(7,"Sábado", "Sábado", "sabado");
	
	private Semana(int valor, String nome, String nomeReduzido, String codI18n){
		this.valor=valor;
		this.nome=nome;
		this.nomeReduzido=nomeReduzido;
		this.codI18n=codI18n;
	}
	
	public int getValor() {return valor;}
	public String getNome() {return nome;}
	public String getNomeReduzido() {return nomeReduzido;}
	public String getcodI18n(){return codI18n;}
	
	private int valor;
	private String nome;
	private String nomeReduzido;
	private String codI18n;
}
