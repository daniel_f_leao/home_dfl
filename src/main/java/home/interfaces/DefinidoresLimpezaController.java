package home.interfaces;
import java.util.Date;
import java.util.List;


public interface DefinidoresLimpezaController{

	public void informarNaoLimpeza(Date date, boolean repetir);
	public List<Date> cincoUltimasDatas();
	public void informarLimpado(int id);
}
