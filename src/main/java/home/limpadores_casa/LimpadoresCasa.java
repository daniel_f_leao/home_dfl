package home.limpadores_casa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import home.pessoa.Pessoa;
@Entity(name="limpadores_casa")
@Table
public class LimpadoresCasa implements Serializable{

	private static final long serialVersionUID = -8451036413820102448L;
	@Id
	@GeneratedValue
	private int id;
	@OneToOne
	private Pessoa p1;
	@OneToOne
	private Pessoa p2;
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date data;
	@Column(nullable=false)
	private boolean limpado;
	
	
	public int getId() {return id;}
	public Pessoa getP1() {return p1;}
	public Pessoa getP2() {return p2;}
	public Date getData() {return data;}
	public boolean isLimpado(){return limpado;}
	
	public void setId(int id) {this.id = id;}
	public void setP1(Pessoa p1) {this.p1 = p1;}
	public void setP2(Pessoa p2) {this.p2 = p2;}
	public void setData(Date data) {this.data = data;}
	public void setLimpado(boolean limpado){this.limpado=limpado;}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LimpadoresCasa other = (LimpadoresCasa) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
