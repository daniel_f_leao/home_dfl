package home.limpadores_casa;

import java.util.Date;
import java.util.List;

import home.pessoa.Pessoa;

public interface LimpadoresCasaDAO {

	public List<LimpadoresCasa> cincoUltimasVezes();
	public List<LimpadoresCasa> todasVezes();
	public List<LimpadoresCasa> cincoUltimasVezesDaPessoa(Pessoa p1);
	public List<LimpadoresCasa> todasVezesDaPessoa(Pessoa p1);
	public List<LimpadoresCasa> cincoUltimasVezesDasPessoas(Pessoa p1,Pessoa p2);
	public List<LimpadoresCasa> todasVezesDasPessoas(Pessoa p1, Pessoa p2);
	public LimpadoresCasa buscarPorData(Date data);
	public LimpadoresCasa buscarPorId(int id);
	public List<LimpadoresCasa> proximasVezes(Date date);
	public void atualizarLimpadoresCasa(LimpadoresCasa limpadores);
}
