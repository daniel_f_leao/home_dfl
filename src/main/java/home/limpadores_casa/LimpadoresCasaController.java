package home.limpadores_casa;

import home.DAOFactory;
import home.interfaces.DefinidoresLimpezaController;
import home.util.Parametros;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class LimpadoresCasaController implements Serializable, DefinidoresLimpezaController{

	private static final long serialVersionUID = -4843429648455791648L;
	
	private LimpadoresCasaDAO dao;
	public LimpadoresCasaController() {
		dao = DAOFactory.getLimpadoresCasaDAO();
	}
	
	public List<LimpadoresCasa> getUltimas5Vezes(){
		return dao.cincoUltimasVezes();
	}
	
	public List<LimpadoresCasa> getTodasLimpadas(){
		return dao.todasVezes();
	}
	/**
	 * Busca por dias de limpezas nos sabados e nos domingos 
	 * @return
	 */
	public LimpadoresCasa proximoLimparCasa(){
		Calendar c = Calendar.getInstance();
//		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);//Volta pro começo da semana(Domingo)
//		c.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR)+6);//Adiciona 6 dias para ir pro sabado
//		LimpadoresCasa retorno = dao.buscarPorData(c.getTime());
//		if (retorno == null){
//			c.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR)+1);//Adiciona 1 dia para ir pro domingo
//			retorno = dao.buscarPorData(c.getTime());
//		}
		LimpadoresCasa retorno = null;
		List<LimpadoresCasa> lista = dao.proximasVezes(c.getTime());
		if(lista!=null && !lista.isEmpty())
			retorno = lista.get(Parametros.PRIMEIRA_POSICAO_VETOR);
		return retorno;
	}
	
	@Override
	public void informarNaoLimpeza(Date date, boolean repetir){
		LimpadoresCasa limpadores = dao.buscarPorData(date);
		limpadores.setLimpado(false);
		dao.atualizarLimpadoresCasa(limpadores);
		if(repetir){
			List<LimpadoresCasa> lista = dao.proximasVezes(date);
			for(LimpadoresCasa limpadoresDaLista: lista){
				Calendar c = Calendar.getInstance();
				c.setTime(limpadoresDaLista.getData());
				c.add(Calendar.DAY_OF_YEAR, 7);
				limpadoresDaLista.setData(c.getTime());
				dao.atualizarLimpadoresCasa(limpadoresDaLista);
			}
		}
	}
	
	@Override
	public List<Date> cincoUltimasDatas() {
		List<Date> datas = new ArrayList<Date>();
		for(LimpadoresCasa limpadores: this.getUltimas5Vezes()){
			datas.add(limpadores.getData());
		}
		return datas;
	}
	
	@Override
	public void informarLimpado(int id) {
		LimpadoresCasa limpadores = dao.buscarPorId(id);
		if(limpadores!=null){
			limpadores.setLimpado(true);
			dao.atualizarLimpadoresCasa(limpadores);
		}
	}
}
