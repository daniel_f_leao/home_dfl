package home.limpadores_casa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import home.pessoa.Pessoa;

public class LimpadoresCasaHibernateDAO implements LimpadoresCasaDAO, Serializable{
	
	private static final long serialVersionUID = 890484874524674717L;
	private Session session;
	public void setSession(Session session) {
		this.session = session;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LimpadoresCasa> cincoUltimasVezes() {
		Criteria c = session.createCriteria(LimpadoresCasa.class);
		c.add(Restrictions.le("data", new Date()));
		return c.addOrder(Order.desc("data")).setMaxResults(5).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LimpadoresCasa> todasVezes() {
		Criteria c = session.createCriteria(LimpadoresCasa.class);
		return c.addOrder(Order.desc("data")).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LimpadoresCasa> cincoUltimasVezesDaPessoa(Pessoa pessoa) {
		Criteria c = session.createCriteria(LimpadoresCasa.class);
		c.add(Restrictions.or(
				Restrictions.eq("p1.id", pessoa.getId()),
				Restrictions.eq("p2.id", pessoa.getId())));
		c.add(Restrictions.le("data", new Date()));
		return c.addOrder(Order.desc("data")).setMaxResults(5).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LimpadoresCasa> todasVezesDaPessoa(Pessoa pessoa) {
		Criteria c = session.createCriteria(LimpadoresCasa.class);
		c.add(Restrictions.or(
				Restrictions.eq("p1.id", pessoa.getId()),
				Restrictions.eq("p2.id", pessoa.getId())));
		return c.addOrder(Order.desc("data")).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LimpadoresCasa> cincoUltimasVezesDasPessoas(Pessoa p1, Pessoa p2) {
		Criteria c = session.createCriteria(LimpadoresCasa.class);
		c.add(Restrictions.or(
				Restrictions.and(Restrictions.eq("p1.id", p1.getId()),Restrictions.eq("p2.id", p2.getId())),
				Restrictions.and(Restrictions.eq("p2.id", p1.getId()),Restrictions.eq("p1.id", p2.getId()))));
		c.add(Restrictions.le("data", new Date()));
		return c.addOrder(Order.desc("data")).setMaxResults(5).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LimpadoresCasa> todasVezesDasPessoas(Pessoa p1, Pessoa p2) {
		Criteria c = session.createCriteria(LimpadoresCasa.class);
		c.add(Restrictions.or(
				Restrictions.and(Restrictions.eq("p1.id", p1.getId()),Restrictions.eq("p2.id", p2.getId())),
				Restrictions.and(Restrictions.eq("p2.id", p1.getId()),Restrictions.eq("p1.id", p2.getId()))));
		return c.addOrder(Order.desc("data")).list();
	}
	
	@Override
	public LimpadoresCasa buscarPorData(Date data) {
		Criteria c = session.createCriteria(LimpadoresCasa.class);
		return (LimpadoresCasa) c.add(Restrictions.eq("data", data)).uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<LimpadoresCasa> proximasVezes(Date date) {
		Criteria c = session.createCriteria(LimpadoresCasa.class);
		c.add(Restrictions.gt("data", date));
		c.addOrder(Order.asc("data"));
		return c.list();
	}
	
	@Override
	public void atualizarLimpadoresCasa(LimpadoresCasa limpadores) {
		session.saveOrUpdate(limpadores);
	}
	
	@Override
	public LimpadoresCasa buscarPorId(int id) {
		return (LimpadoresCasa) session.get(LimpadoresCasa.class, id);
	}
}
