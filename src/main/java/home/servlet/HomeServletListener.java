package home.servlet;

import home.beans.AplicacaoBean;
import home.util.Parametros;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class HomeServletListener implements ServletContextListener{
	
	
	private void destruirThreadsAplicacaoBean(Object obj){
		if(obj instanceof AplicacaoBean){
			AplicacaoBean app = (AplicacaoBean) obj;
			if(app.getGerenciador()!=null){
				app.getGerenciador().terminar();
			}
		}
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		Object supostoAplicacaoBean = sce.getServletContext().getAttribute(Parametros.NOME_ADMINISTRADOR_APLICACAO_BEAN);
		destruirThreadsAplicacaoBean(supostoAplicacaoBean);
	}
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {}
}
