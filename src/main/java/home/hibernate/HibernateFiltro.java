package home.hibernate;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class HibernateFiltro implements Filter{

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		Session sessao=null;
		Transaction transaction=null;
		try{
			sessao = HibernateFactory.getSession().getCurrentSession();
			transaction = sessao.beginTransaction();
			chain.doFilter(request, response);
			transaction.commit();
		}
		catch(HibernateException hEx){
			if(transaction!=null && transaction.isActive()){
				transaction.rollback();
			}
			hEx.printStackTrace();
		}
		finally{
			if(sessao!=null && sessao.isOpen())
				sessao.close();
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
}
