package home.hibernate;

import java.io.Serializable;

import org.hibernate.SessionFactory;
import org.hibernate.SessionFactoryObserver;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.registry.internal.StandardServiceRegistryImpl;
import org.hibernate.cfg.Configuration;

public class HibernateFactory implements Serializable{

	private static final long serialVersionUID = 2507904876254173692L;
	private static SessionFactory session = criarSessao();
	
	private static SessionFactory criarSessao(){
		try{
			Configuration cfg = new Configuration();
			cfg.configure("hibernate.cfg.xml");
			StandardServiceRegistryBuilder registradorServico = new StandardServiceRegistryBuilder();
			registradorServico.applySettings(cfg.getProperties());
			final StandardServiceRegistry servico = registradorServico.build();
			cfg.setSessionFactoryObserver(new SessionFactoryObserver() {
				private static final long serialVersionUID = 1L;
				@Override
				public void sessionFactoryCreated(SessionFactory factory) {}
				
				@Override
				public void sessionFactoryClosed(SessionFactory factory) {
					((StandardServiceRegistryImpl)servico).destroy();
				}
			});
			return cfg.buildSessionFactory(servico);
		}
		catch(Throwable e){
			System.out.println("Criação inicia do objeto SessionFactory falhou. Mensagem: " + e.getMessage());
			throw new ExceptionInInitializerError(e);
		}
	}
	
	public static SessionFactory getSession() {
		return session;
	}
}
